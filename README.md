![N|Solid](logo_ioasys.png)

# Desafio React Native - ioasys

Este documento `README.md` tem como objetivo fornecer as informações necessárias para realização do projeto Empresas.
## bibliotecas utilizadas

### react-native-svg-transformer 
facilita o trabalho com svg, tratando eles como componentes.

### expo-secure-store
adicionada para manusear os dados no storage, pois o AsyncStorage esta obsoleto.

### axios
adicionada para facilitar nas requisições feitas ao backend


## Available Scripts  
### yarn install ou npm install
instala todas as dependencias do projeto

### expo start
inicia o expo em modo de desenvolvimento 

### yarn start ou npm start 
para iniciar o react-native em modo de desenvolvimento

# Gerando a build
### expo build:android
para compilar o projeto e gerar o apk para Android 

### expo build:ios
para compilar o projeto e gerar o apk para IOS 

ele solicitará a criação de uma nova conta se você não tiver uma conta expo  **expo build:status** para saber o status do seu aplicativo e o aplicativo na fila pode levar cerca de 30 minutos para gerar o arquivo apk.

 **será gerado um link onde você pode baixar o apk ou ipa.**

 